<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Redis;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users, makes validation by special key and
    | set hash id and token in redis
    |
    */


    /**
     * Constant keys for registration and auth routes.
     *
     * @var string
     */
    private $REG_KEY = 'rNkJGSL1sg@Jbz@iFWV8|4fB5lP{n#Z%HGGQtQOb';
    private $AUTH_KEY = 'rNkJGSL1sg@Jbz@iFWV8|4fB5lP{n#Z%HGGQtQOb';

    /**
     * Check incoming registration or auth key.
     *
     * @param  array  $data, $key
     * @return bool
     */
    protected function validator(array $data, $key)
    {
        return $data['key'] == $key;
    }

    /**
     * Set a new user token in redis.
     *
     * @param  $id, $token
     */
    protected function setUserInRedis ($id, $token)
    {
        $redis = Redis::connection();
        $redis->set(md5($id), $token);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User's id and access token
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'country_id' => (int) $data['country_id'],
            'password' => $data['password'],
            'phone_number' => $data['phone_number'],
            'access_token' => str_random(16)
        ]);

        $this->setUserInRedis($user->id, $user->access_token);

        return [
            'user_id' => $user->id,
            'access_token' => $user->access_token
        ];
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->json()->all(), $this->REG_KEY);

        if($validator) {
            $newUser = $this->create($request->json()->all());
            return response()->json($newUser);
        } else {
            return response()->json([
                'Error' => 'Wrong registration key'
            ]);
        }
    }

    /**
     * Handle an authentication attempt.
     *
     * @return array with result
     */

    public function authenticate(array $data)
    {
        $user = User::where('phone_number', '=', $data['phone_number'])->first();

        if ($user && $user->password == $data['password']) {
            return [
                'user_id' => $user->id,
                'access_token' => $user->access_token,
                'user_status' => $user->user_status
            ];
        } else {
            return [
                'Error' => 'Wrong login or password'
            ];
        }
    }

    public function login(Request $request)
    {
        $validator = $this->validator($request->json()->all(), $this->AUTH_KEY);

        if($validator) {
            $authResult = $this->authenticate($request->json()->all());
            return response()->json($authResult);
        } else {
            return response()->json([
                'Error' => 'Wrong authenticate key'
            ]);
        }
    }
}
