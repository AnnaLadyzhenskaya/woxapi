<?php

namespace App\Http\Controllers\Map;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Http\Traits\RequestValidator;
use Illuminate\Routing\Route;

class MapController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Map Controller
    |--------------------------------------------------------------------------
    |
    | This controller provides getting information about all active cars.
    | Controller uses a trait with request validation logic common for requests
    |
    */

    use RequestValidator;

    public function getMapInfo(Request $request)
    {
        $this->validateRequest($request, ['access_token', 'lat', 'lng']);
        if($this->error) {
            return response()->json(['Error' => $this->error]);
        } else {
            $query = $this->mapInfoQuery();
            return response()->json(['cars' => $query]);
        }

    }

    /**
     * Method gets all cars with active status.
     *
     * @return array with result
     */
    public function mapInfoQuery ()
    {
        $cars =  DB::table('cars_info')
            ->join('cars_costs', 'cars_costs.car_id', '=', 'cars_info.id')
            ->join('drivers_info', 'drivers_info.driver_id', '=', 'cars_info.driver_id')
            ->where('cars_info.status', '=', 1)
            ->select('cars_info.*', 'cars_costs.currency', 'cars_costs.planting_costs', 'cars_costs.costs_per_1',
            'drivers_info.driver_phone')
            ->get();

        /**
         * Car location as object with key "location".
         */
        foreach ($cars as $item) {
            $location = DB::table('cars_location')
                ->where('car_id', '=', $item->id)
                ->select('lat', 'lon')
                ->get();
            $item->location = $location[0];
        }
        return $cars;
    }
}
