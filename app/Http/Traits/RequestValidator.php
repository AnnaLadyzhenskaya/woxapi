<?php
/**
 * Created by PhpStorm.
 * User: myska
 * Date: 13.11.16
 * Time: 20:58
 */

namespace App\Http\Traits;

use App\User;

trait RequestValidator
{
    protected $error;

    /**
     * Method alternatively checks request array on errors
     *
     * @param $request
     * @param $params array of parameters, that must be defined in request
     * @return string
     */
    public function validateRequest ($request, $params)
    {
        if (!$this->checkCorrectParams($request, $params)) {
            $this->error = "Incorrect parameters of method";
        } else if(!$this->checkAccessToken($request['access_token'])) {
            $this->error = "Invalid access token: ".$request['access_token'];
        } else if($request['user_id'] && !$this->checkUserId($request['user_id'])) {
            $this->error = "User id not found";
        }
        return $this->error;
    }

    /**
     * Check on valid access_token
     *
     * @param $token
     * @return bool
     */
    protected function checkAccessToken($token)
    {
        return User::where('access_token', '=', $token)->first();
    }

    /**
     * Check if all parameters are set in request
     *
     * @param $request
     * @param $params
     * @return bool
     */
    protected function checkCorrectParams($request, $params)
    {
        for ($i = 0; $i < count($params); $i += 1) {
            if (!array_key_exists($params[$i], $request->query())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check on valid user_id
     *
     * @param $id
     * @return bool
     */
    protected function checkUserId($id)
    {
        return User::where('id', '=', $id)->first();
    }

}