<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

//Route::auth();
Route::post('/api/registration', 'Auth\AuthController@register');
Route::post('/api/auth', 'Auth\AuthController@login');
Route::get('/api/getMapInfo', 'Map\MapController@getMapInfo');
