<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id');
            $table->string('color');
            $table->string('reg_number');
            $table->string('year');
            $table->string('brand');
            $table->string('model');
            $table->string('photo');
            $table->integer('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cars_info');
    }
}
