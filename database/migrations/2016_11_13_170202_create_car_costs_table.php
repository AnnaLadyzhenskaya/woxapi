<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id');
            $table->string('currency');
            $table->integer('planting_costs');
            $table->integer('costs_per_1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('car_costs');
    }
}
